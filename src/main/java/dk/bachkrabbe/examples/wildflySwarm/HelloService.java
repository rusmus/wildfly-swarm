package dk.bachkrabbe.examples.wildflySwarm;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class HelloService {

    public String getGreeting(String name){

        if(name == null || name.trim().length() == 0)
            name = "unkown dude";

        return String.format("Greetings, %s.", name.trim());

    }

}
