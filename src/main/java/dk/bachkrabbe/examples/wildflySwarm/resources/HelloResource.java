package dk.bachkrabbe.examples.wildflySwarm.resources;

import dk.bachkrabbe.examples.wildflySwarm.HelloService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("hello")
@ApplicationScoped
public class HelloResource {

    private HelloService helloService;

    public HelloResource() { }

    @Inject
    public HelloResource(HelloService helloService) {

        this.helloService = helloService;
    }

    @GET
    @Path("/{name}")
    public String hello(@PathParam("name") String name) {
        return helloService.getGreeting(name);
    }
}
