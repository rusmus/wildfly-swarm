FROM openjdk:8u131-jdk-alpine

COPY ./ /

RUN ./gradlew build
WORKDIR /build/libs/

EXPOSE 8080

CMD ["java", "-jar", "-Xms256m", "-Xmx512m", "-Djava.net.preferIPv4Stack=true", "wildfly-swarm-swarm.jar"]
